const problem4 = require("./problem4");

const problem5 = (inventory, year) => {
let i = inventory;
  if (!i || !year) return [];
  const years = problem4(inventory);
  const oldCars = [];
  for (let a = 0; a < years.length; a++) {
    if (years[a] < year) {
      oldCars.push(i[a]);
    }
  }
  return oldCars;
};

module.exports = problem5;
