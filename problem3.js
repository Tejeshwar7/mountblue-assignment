const problem3 = (inventory) => {
let i = inventory;
  if (!i) return [];
  return i.sort((x, y) => {
    var P = x.car_model.toLowerCase();
    var Q = y.car_model.toLowerCase();
    if (P < Q) {
      return -1;
    }
    if (P > Q) {
      return 1;
    }

    // names must be equal
    return 0;
  });
};

module.exports = problem3;
