const problem2 = (inventory) => {
let i = inventory;
  if (!i) return {};
  const lastCar = i[i.length - 1];
  return lastCar;
};

module.exports = problem2;
