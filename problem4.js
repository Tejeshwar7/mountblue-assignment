const problem4 = (inventory) => {
let i = inventory;
  if (!i) return [];
  const years = [];
  for (let a = 0; a < i.length; a++) {
    years.push(i[a].car_year);
  }
  return years;
};

module.exports = problem4;
