const problem1 = (inventory, id) => {
let i= inventory;
  if (!i || !id) return [];

  for (let a = 0; a < i.length; a++) {
    if (i[a].id == id) {
      return inventory[a];
    }
  }

  return [];
};

module.exports = problem1;
